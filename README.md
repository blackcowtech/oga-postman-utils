# OGA Postman utils

## Introduction

A collection of requests that are designed to make it easy to interface with OGA by taking care of the repeated tasks such as E-Tag management or `latest-gameplay` rel tracking.

## Quick start

1. [Install Postman here](https://www.postman.com/downloads/)
2. Import the `OGA.postman_collection.json` & `Example Engine.postman_environment.json` files to postman using the import button. [See docs here on how to import postman environment and collection](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#importing-data-into-postman)
3. Select the `Example Engine` or `Punto Banco` environment to play from the drop down at the top left, for example `example-slot` or `punto-banco`.
4. Edit the `OGA_BASE_URL` to point to your OGA of choice, by default this will be `0.0.0.0:6543`
5. Navigate to the `search` request and hit send.
6. Navigate to the `definition` request and hit send to retrieve a definition.
7. Navigate to any of the other request's to continue sending requests or completing play requests.
8. To see a visualization of the request, navigate to the `Visualize` tab in the response section to view a rendering of this request.

> *📝* Every new request to `search` will cause a new player to be generated, which will require a new call to definition.

## Features

1. **Search** - Allows for a configurable search using environment variables to set search criteria. Also generates a token per search if you need to use a new player.
2. **Game URL** - Based on the result returned from search will use the host-game URL to send requests to the RGS in future requests.
3. **Forcing** - Contains force string examples on how to send forcer requests.
4. **Client player state save** - Shows an example usage of the state save mechanism that clients can use.
5. **Example Slot** - Supports full plays of example slot:
    - new-game-spin - Logic built in to prevent requests being sent erroneously depending on the state of the game.
    - continue-freespin - Allows the user to play through freespins.
    - new-game-gamble - Alllows the user to play a gamble.
    - **Visualize** - Contains a simple rendering of a `5 \* 3` slot, should work with most OGLE_Slot engine's that use a `5 \* 3` grid to show a basic rendering.
6. **Punto Banco** - Supports full plays of the punto banco engine
7. **Roulette** - Supports full plays of both US and European roulette.
8. **Blackjack** - Supports full plays of single and multi-hand US Blackjack
9.  **E-tag management** - Takes care of tracking what your current E-tag is so you don't need to read the last response to determine what it was.
10. **Player** - Supports all of the generic link relations that OGA renders for a player
    - **Session** - Allows for the user to close or retrieve the current session.
    - **Balance** - Allows for retrieving the current players balance.